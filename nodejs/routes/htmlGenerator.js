var express = require('express');
var router = express.Router();

// GET home page
router.get('/', function(req, res, next) {
  res.redirect(301, '/generated/categories');
});

// GET list of all categories page
router.get('/categories', function(req, res, next) {
  req.services.categories.listAll((array) =>{
      res.render('categories', { title: 'Categories', categories:  array});
  });
});

// GET list of all events of a categories page
router.get('/categories/:categorieID/events', function(req, res, next) {
  let categorieID = req.params.categorieID;
  req.services.events.listAllByQuery({"categorie": categorieID}, (array) => {
    console.log(array);
    res.render('events', { title: categorieID, eventsArray: array });
  })
});

// GET list of all all events page
router.get('/events', function(req, res, next) {
  req.services.events.listAll((array) => {
    res.render('events', { title: 'All events', eventsArray: array });
  });
});

// GET create new event page
router.get('/events/new', function(req, res, next) {
  res.render('createEvent', { title: 'Create events' });
});

// GET one specific event page
router.get('/events/:eventID', function(req, res, next) {
  let eventID = req.params.eventID;
  req.services.events.findById(
    eventID,
    (item) => { res.render('event', { title: 'All events', event: item });  }
  );
});

// GET list of all events created by user page
router.get('/users/:username/events', function(req, res, next) {
  let creator = req.params.username;
  req.services.events.listAllByQuery(
    {"creator":creator},
    (array) => { res.render('events', { title: 'My events', eventsArray: array }); }
  );
});

module.exports = router;
