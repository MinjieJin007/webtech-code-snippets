var express = require('express');
var router = express.Router();

// POST create event
router.post('/', function(req, res, next) {
  console.log(req.body);
  req.services.events.create(req.body, (id) => {
    console.log(id);
    res.setHeader("EventID", id);
    res.status(201).send('Created');
  });
});

// GET list of all event
router.get('/', function(req, res, next) {
  req.services.events.listAll((array) => res.json(array));
});

// GET specific event identified by ID
router.get('/:eventId', function(req, res, next) {
  let eventId = req.params.eventId;
  req.services.events.findById( eventId, (event) => {
    res.status(200).send(event);
  });
});

/* We don't need that function for our project because we don't have authentication and can't decide if a user is really the owner of the event
// PUT update specific event
router.put('/:eventId', function(req, res, next) {
  let eventId = req.params.eventId;
  // Implementation
  res.status(204).send('No Content');
});
*/

/* We don't need that function for our project because we don't have authentication and can't decide if a user is really the owner of the event
// DELETE specific event
router.delete('/:eventId', function(req, res, next) {
  let eventId = req.params.eventId;
  // Implementation
  res.send(eventId);
});
*/

module.exports = router;
