import { Component, OnInit } from '@angular/core';
import { Flatmate } from '../flatmate';
import { Purchase } from '../purchase';
import { HttpCommunicatorService } from '../http-communicator.service';

@Component({
  selector: 'app-add-purchase',
  templateUrl: './add-purchase.component.html',
  styleUrls: ['./add-purchase.component.css']
})
export class AddPurchaseComponent implements OnInit {

    flatmates: Flatmate[] = [];
    newPurchase: Purchase = {
        id: -1,
        title: '',
        description: '',
        cost: undefined,
        date: '',
        boughtBy: '',
        boughtFor: []
    };

    purchaseCost: number;
    
    purchaseCreated: boolean = false;
    
    addPurchase() {
      this.newPurchase.cost = this.purchaseCost * 100;
      console.log(this.newPurchase);
      this.http.addPurchase(this.newPurchase).subscribe(resp => {
        if (resp.status === 200) {
          this.purchaseCreated = true;
        }
      });
    }

    constructor(private http: HttpCommunicatorService) {
      //this.http.getAllFlatmates().subscribe(data => this.flatmates = data);
      this.http.getAllFlatmates().subscribe(resp => {
        if (resp.status === 200) {
          this.flatmates = resp.body;
          this.newPurchase.boughtBy = this.flatmates[0].name;
        }
      });
    }

  ngOnInit() {
  }

}
