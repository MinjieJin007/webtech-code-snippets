<?php
require_once(__DIR__.'/Page/PageInterface.php');

use Page\PageInterface;

class Renderer {

    public function render( Page\PageInterface $page ){
        ob_start( );
        $this->showViewScript(
            $page->getViewScript( ),
            $page->getViewVariables( )
        );
        $content = ob_get_clean( );
        return $content;
    }

    public function showViewScript( $viewScript, $variables ) {
        require( $viewScript );
    }

}
