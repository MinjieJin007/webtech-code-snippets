<?php
namespace Page;

require_once(__DIR__ .'/PageInterface.php');
use \Page\PageInterface;

class Seite1 implements PageInterface {
    public function getTitle() {
        return 'MiniBsp-Startseite';
    }

    public function getViewScript() {
        return __DIR__.'/../../view/Seite1.phtml';
    }

    public function getViewVariables() {
        return ['name' => 'WebTech-Audience'];
    }
}
?>
